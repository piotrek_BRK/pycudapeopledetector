from scipy import signal as sig
import scipy
import cv2
import numpy as np
import math
 
class Detector():
	
	def __init__(self):
		self.numBins = 9
		self.numHorizCells = 8
		self.numVertCells = 16
		self.cellSize = 8
		self.winSize = (130, 66)
		self.threshold = 0.4
		self.suppressionThreshold = 0.5
		self.suppressionThreshold2 = 0.9
		self.theta = np.fromfile("hogThetaCoefs.csv",
								 dtype = np.float32, count = -1, sep = ",")		

	def countWindows(self, inImg, scaleRange):
		#COUNTWINDOWS Counts the number of possible detection windows in the image.	
		#For each image scale in 'scaleRange', count the number of unique detection windows
		#that fit within the image. This is used simply to predict the number of windows
		#that will need to be processed in the image.
		#Parameters:
		#	img			- The image to be searched.
		#	scaleRange	- Vector containing all scales to be searched.
		#Returns:
		#	windowCounts - Vector containing the number of detector windows in
		#				   the image at each of the scales in 'scaleRange'.

		#Get the image dimensions. Make sure to read all three dimensions,
		#or 'origImgWidth' will be wrong.
		
		origImgHeight, origImgWidth, depth = inImg.shape
	
		#Initialize the windowCounts array.
		windowCounts = np.zeros(len(scaleRange))
	
		#Try progressively smaller scales until a window doesn't fit.
		for i in range(0, len(scaleRange)):		
			#Get the next scale.
			scale = scaleRange[i]		
			#Compute the scaled img size.
			imgWidth = origImgWidth * scale
			imgHeight = origImgHeight * scale
	
			#Compute the number of cells horizontally and vertically for the image.
			numHorizCells = math.floor((imgWidth - 2) / self.cellSize)
			numVertCells = math.floor((imgHeight - 2) / self.cellSize)
		
			#Break the loop when the image is too small to fit a window.
			if ((numHorizCells < self.numHorizCells) or (numVertCells < self.numVertCells)):
				break		
		
			#The number of windows is not quite equal to the number of cells, since
			#you have to stop when the edge of the detector window hits the edge of
			#the image.
			numHorizWindows = numHorizCells - self.numHorizCells + 1
			numVertWindows = numVertCells - self.numVertCells + 1
		
			#Compute the number of windows at this image scale.
			windowCounts[i] = numHorizWindows * numVertWindows	  
			
		return windowCounts

	def getHistogram(self, magnitudes, angles):
 		# GETHISTOGRAM Computes a histogram for the supplied gradient vectors.
 		# This function takes the supplied gradient vectors and places them into a
 		# histogram with 'numBins' based on their unsigned orientation. 
 		# Parameters:
 		#	magnitudes - A column vector storing the magnitudes of the gradient vectors.
 		#   angles	 - A column vector storing the angles in radians of the gradient vectors
 		# Returns:
 		#	A row vector of length 'numBins' containing the histogram.
 
 		# Compute the bin size in radians. 180 degress = pi.
 		binSize = np.pi / self.numBins
 
 		# The angle values will range from 0 to pi.
 		minAngle = 0
 
 		# Make the angles unsigned by adding pi (180 degrees) to all negative angles.
 		for i in range(0, len(angles)):
 			if (angles[i] < 0):
 				angles[i] = angles[i] + np.pi
 		
 		# The gradient angle for each pixel will fall between two bin centers.
 		# For each pixel, we split the bin contributions between the bin to the left
 		# and the bin to the right based on how far the angle is from the bin centers.
 
 		# For each pixel's gradient vector, determine the indeces of the bins to the
 		# left and right of the vector's angle.
 		leftBinIndex = np.round((angles - minAngle) / binSize)
 		rightBinIndex = leftBinIndex + 1
 
 		# For each pixel, compute the center of the bin to the left.
 		leftBinCenter = ((leftBinIndex - 0.5) * binSize) - minAngle
 
 		# For each pixel, compute the fraction of the magnitude
 		# to contribute to each bin.
 		rightPortions = angles - leftBinCenter
 		leftPortions = binSize - rightPortions
 		rightPortions = rightPortions / binSize
 		leftPortions = leftPortions / binSize
		
 		leftBinIndex = (leftBinIndex == 0).choose(leftBinIndex, self.numBins) 
 		rightBinIndex = (rightBinIndex == (self.numBins + 1)).choose(rightBinIndex, 1)

 		leftBinIndex = np.subtract(leftBinIndex, 1) 
 		rightBinIndex = np.subtract(rightBinIndex, 1) 
 
 		# Create an empty row vector for the histogram.
 		H = np.zeros(self.numBins)
 
 		# For each bin index...
 		for i in range(self.numBins):
 			# Find the pixels with left bin == i
 			pixels = (leftBinIndex == i)
 				
 			# For each of the selected pixels, add the gradient magnitude to bin 'i',
 			# weighted by the 'leftPortion' for that pixel.
 			H[i] = H[i] + np.sum(leftPortions[pixels].conj().transpose() * magnitudes[pixels])
 						
 			# Find the pixels with right bin == i
 			pixels = (rightBinIndex == i)
 				
 			# For each of the selected pixels, add the gradient magnitude to bin 'i',
 			# weighted by the 'rightPortion' for that pixel.
 			H[i] = H[i] + np.sum(rightPortions[pixels].conj().transpose() * magnitudes[pixels])
 
 		return H

	def getHist4Img(self, img):		
		#  getHist4Img Calculats histograms for ALL cells in an image.
		#  This function is part of a technique for optimizing the process of 
		#  computing HOG descriptors over a search image.
		#  Parameters:   
		#    img            - The input image to be searched.   
		#  Returns:
		#    histograms - 3D matrix of histograms for all cells in the image:
		#                 The first two dimensions index the matrix by cell number,
		#                 and the third dimension contains the histograms.
		#                 For example, the histogram for the cell at (3, 5) is 
		#                 given by histograms(3, 5, :)
		#    xoffset,   - If the image dimensions are not even multiples of the 
		#    yoffset      cell size, then we will first crop the image. We try to
		#                 center the crop window within the image. xoffset and 
		#                 yoffset are the coordinates of the top-left-corner of the
		#                 crop window relative to the original image.

		# === Crop The Image ===

		imgHeight = img.shape[0]
		imgWidth = img.shape[1]

		# Compute the number of cells horizontally and vertically for the image.
		numHorizCells = np.floor((imgWidth - 2) / self.cellSize)
		numVertCells = np.floor((imgHeight - 2) / self.cellSize)

		# Compute the new image dimensions.
		newWidth = int((numHorizCells * self.cellSize) + 2)
		newHeight = int((numVertCells * self.cellSize) + 2)

		# Divide the left-over pixels in half to center the crop region.
		xoffset = int(np.around((imgWidth - newWidth) / 2) + 1)
		yoffset = int(np.around((imgHeight - newHeight) / 2) + 1)

		# Crop the image.
		img = img[yoffset : (yoffset + newHeight - 1), xoffset : (xoffset + newWidth - 1)]

		# === Compute Gradient Vectors ===
		# Compute the gradient vector at every pixel in the image.

		# Create the operators for computing image derivative at every pixel.
		hx = np.array([[0,0,0],[-1,0,1],[0,0,0]])		
		hy = hx.conj().transpose()

		# Compute the derivative in the x and y direction for every pixel.		
		dx = sig.convolve2d(img.astype(float), np.rot90(hx, 2), mode = "same")
		dy = sig.convolve2d(img.astype(float), np.rot90(hy, 2), mode = "same")		

		# Remove the 1 pixel border.
		dx = dx[1 : (newHeight - 1), 1 : (newWidth - 1)]
		dy = dy[1 : (newHeight - 1), 1 : (newWidth - 1)]

		# Convert the gradient vectors to polar coordinates (angle and magnitude).
		angles = np.arctan2(dy, dx)
		magnit = np.power( (np.power(dy, 2) + np.power(dx, 2)), 0.5)

		# === Compute Cell Histograms ===
		# Compute the histogram for every cell in the image. We'll combine the cells
		# into blocks and normalize them later.

		# Create a three dimensional matrix to hold the histogram for each cell.
		histograms = np.zeros((int(numVertCells), int(numHorizCells), int(self.numBins)))

		# For each cell in the y-direction...
		for row in range(0, int(numVertCells) - 1):
			
			# Compute the row number in the 'img' matrix corresponding to the top
			# of the cells in this row.
			rowOffset = row * self.cellSize
			
			# For each cell in the x-direction...
			for col in range(0, int(numHorizCells) - 1):
			
				# Select the pixels for this cell.
				
				# Compute column number in the 'img' matrix corresponding to the left
				# of the current cell.
				colOffset = col * self.cellSize
				
				# Compute the indices of the pixels within this cell.
				rows = np.arange(rowOffset, rowOffset + self.cellSize)
				cols = np.arange(colOffset, colOffset + self.cellSize)
				
				# Select the angles and magnitudes for the pixels in this cell.
				cellAngles = angles[rows, cols]
				cellMagnitudes = magnit[rows, cols]
			
				# Compute the histogram for this cell.
				# Convert the cells to column vectors before passing them in.
				histograms[row + 1, col + 1, :] = self.getHistogram(cellMagnitudes[:], cellAngles[:])
		
		return (histograms, xoffset, yoffset)

	def getDescFromHist(self, histograms):
		#  getDescFromHist Get descriptor using pre-calculated histograms
		#  This function takes the pre-computed histograms for an image, and
		#  performs the final step, block normalization, to produce the final
		#  descriptor. 
		# Parameters:    
		#    histograms          - The histograms for the HOG descriptor.
		#
		#  Returns:
		#    H  - A fully computed HOG descriptor.

		# Empty vector to store computed descriptor.
		H = np.array([])

		# === Block Normalization ===  

		# Take 2 x 2 blocks of cells and normalize the histograms within the block.
		# Normalization provides some invariance to changes in contrast, which can
		# be thought of as multiplying every pixel in the block by some coefficient.

		# For each cell in the y-direction...
		for row in range(0, self.numVertCells - 1):   
			# For each cell in the x-direction...
			for col in range(0, self.numHorizCells - 1):
			
				# Get the histograms for the cells in this block.
				blockHists = histograms[row : row + 2, col : col + 2, :]
										
				# Put all the histogram values into a single vector (nevermind the 
				# order), and compute the magnitude.
				# Add a small amount to the magnitude to ensure that it's never 0.
				magnitude = np.linalg.norm(blockHists.flatten(1)) + 0.01			
			
				# Divide all of the histogram values by the magnitude to normalize 
				# them.
				normalized = blockHists / magnitude

				# Append the normalized histograms to our descriptor vector.
				H = np.concatenate((H, normalized.flatten(1)), axis = 0 )		
		return H	

	def non_max_suppression(self, boxes, overlapThresh):
		# if there are no boxes, return an empty list
		if len(boxes) == 0:
			return []
	
		# if the bounding boxes integers, convert them to floats --
		# this is important since we'll be doing a bunch of divisions
		if boxes.dtype.kind == "i":
			boxes = boxes.astype("float")
	
		# initialize the list of picked indexes	
		pick = []
	
		# grab the coordinates of the bounding boxes
		x1 = boxes[:,0]
		y1 = boxes[:,1]
		x2 = x1 + boxes[:,2]
		y2 = y1 + boxes[:,3]

		s = boxes[:, boxes.shape[1] - 1]
	
		# compute the area of the bounding boxes and sort the bounding
		# boxes by the bottom-right y-coordinate of the bounding box
		area = (x2 - x1 + 1) * (y2 - y1 + 1)
		idxs = np.argsort(-s)
	
		# keep looping while some indexes still remain in the indexes
		# list
		while len(idxs) > 0:
			# grab the last index in the indexes list and add the
			# index value to the list of picked indexes
			last = len(idxs) - 1
			i = idxs[last]
			pick.append(i)
	
			# find the largest (x, y) coordinates for the start of
			# the bounding box and the smallest (x, y) coordinates
			# for the end of the bounding box
			xx1 = np.maximum(x1[i], x1[idxs[:last]])
			yy1 = np.maximum(y1[i], y1[idxs[:last]])
			xx2 = np.minimum(x2[i], x2[idxs[:last]])
			yy2 = np.minimum(y2[i], y2[idxs[:last]])
	
			# compute the width and height of the bounding box
			w = np.maximum(0, xx2 - xx1 + 1)
			h = np.maximum(0, yy2 - yy1 + 1)
	
			# compute the ratio of overlap
			overlap = (w * h) / area[idxs[:last]]
	
			# delete all indexes from the index list that have
			idxs = np.delete(idxs, np.concatenate(([last],
				np.where(overlap > overlapThresh)[0])))
	
		# return only the bounding boxes that were picked using the
		# integer data type
		return boxes[pick].astype("int")