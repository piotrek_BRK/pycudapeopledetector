from mpi4py import MPI
from scipy import signal as sig
import scipy
import cv2
import numpy as np
import math
from detector import Detector

comm = MPI.Comm.Get_parent()
size = comm.Get_size()
rank = comm.Get_rank()

#print ("Rank = {}, size = {}".format(rank, size))

hDetector = Detector()

## broadcast orginal image copy from master to worker threads

imgSize = np.zeros(3, dtype = "i")

comm.Bcast(imgSize, root = 0)

inputImage = np.zeros((imgSize[0], imgSize[1], imgSize[2]), dtype = "uint8")

comm.Bcast(inputImage, root = 0)

### find Objects function from detector.py taken from detector class to Worker
resultRects = list()		 
scaleRange = np.zeros(64)

comm.Bcast(scaleRange, root = 0)

#Count the total number of windows to be searched.
windowCounts = hDetector.countWindows(inputImage, scaleRange)
totalWindows = np.sum(windowCounts)

#print("Process {} - searching {} detection windows.\n".format(rank, totalWindows))

#Track the number of windows we've processed in this image.
windowCountImg = 0

# For each of the image scales...
for i in range(rank, len(scaleRange), size):            ## Here is the change - each proces takes
                                                        ## it's own scale values 
    #Get the next scale value.
    scale = scaleRange[i] 
    #Stop the search if this scale is too small to fit even a single window.
    if (windowCounts[i] == 0):
        #print("\tImage scale {:.2f} is not large enough for descriptor, stopping search.\n".format(scale))
        continue	

    print("\tProcess {} - Image Scale {:.2f}, {} windows".format(rank, scale, windowCounts[i]))

    #Scale the image.
    if (scale == 1):
        img = inputImage
    else:
        img = cv2.resize(inputImage, (0,0), fx = scale, fy = scale,
                         interpolation = cv2.INTER_AREA) 
    
    #Convert to grayscale by averaging the three color channels.
    img = img.mean(axis = 2)
            
    windowsAtScale = 0

    # === Compute Histograms Over Image ===
    allHistograms, xoffset, yoffset = hDetector.getHist4Img(img)

    cellRow = 0

    #For each row of the image...
    while( (cellRow + hDetector.numVertCells) < allHistograms.shape[0] ):

        #Compute the range of rows to select.
        cellRowRange = np.arange(cellRow, cellRow + hDetector.numVertCells)
        #Reset the cellCol position.
        cellCol = 0
            
        #For each column of the image...
        while( (cellCol + hDetector.numHorizCells) < allHistograms.shape[1]):
    
            # === Get Descriptor ===
            # Compute the range of columns to select.
            cellColRange = np.arange(cellCol, cellCol + hDetector.numHorizCells) 
    
            # Select the histograms for the next detection window.							
            histograms = allHistograms[cellRowRange][:,cellColRange][:]
    
            # Compute the HOG descriptor.
            H = hDetector.getDescFromHist(histograms)
                        
            # Take the transpose to make it a row vector.
            H = H.conj().transpose()

            # === Detect Person ===		
            # Apply the linear SVM.
            p = H.dot(hDetector.theta)

            # If we recognize the histogram as a person...
            if (p > hDetector.threshold):
    
                # === Add Result ===						
                xstart = xoffset + ((cellCol - 0) * hDetector.cellSize)
                ystart = yoffset + ((cellRow - 0) * hDetector.cellSize)
            
                # Compute the detection window coorindate and size 
                # relative to the original image scale.
                origX = round(float(xstart) / scale)
                origY = round(float(ystart) / scale)
                origWidth = round(float(hDetector.winSize[1]) / scale)
                origHeight = round(float(hDetector.winSize[0]) / scale)
            
                # Add the rectangle to the results.
                resultRects.append((origX, origY, origWidth, origHeight, p))						  

            # Increment the count of windows processed.
            windowCountImg = windowCountImg + 1
            windowsAtScale = windowsAtScale + 1
        
            # Move to the next column of the image.
            cellCol = cellCol + 1			
    
        cellRow = cellRow + 1	

## do not execute non_max_suppression - master process will do it - simply send back boxes found
boxesFound = np.zeros(comm.Get_size(), dtype = "i")
boxesFound[rank] = len(resultRects)
comm.Reduce([boxesFound, MPI.INT], None, op = MPI.SUM, root = 0)

comm.gather(np.array(resultRects), root = 0)
#print("Process {} - sending {} boxes of size {}\n".format(rank, len(resultRects), resultRectsArr.shape))
print("\tProcess {} - total windows counted: {}\n".format(rank, windowCountImg))
comm.Disconnect()