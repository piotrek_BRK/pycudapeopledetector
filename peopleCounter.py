import sys
import os.path
import shutil
from mpi4py import MPI
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot
from peopleCounterUI import Ui_MainWindow
import numpy as np
from detector import Detector
import cv2
import time

class Main(QtWidgets.QMainWindow, Ui_MainWindow):
    imgDisplayed = QtGui.QImage()

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)
        self.tBtnFileSelect.clicked.connect(lambda: self.tBtnFileSelect_Clicked())
        self.pBtnRun.clicked.connect(lambda: self.pBtnRun_Clicked())
        self.processesNum = 4
        self.scales = 64

    @pyqtSlot()
    def tBtnFileSelect_Clicked(self):       
        options = QtWidgets.QFileDialog.Options() 
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Choose an image file...", "", "Image Files (*.jpg *.jpeg *.png)")

        self.lEditFileName.setText(fileName)
        imgOrgi = QtGui.QImage(fileName)
        self.imgDisplayed = imgOrgi.scaled(710, 395, QtCore.Qt.KeepAspectRatio)
        self.lImageView.setPixmap(QtGui.QPixmap.fromImage(self.imgDisplayed))
        print("Opened file: {}".format(fileName))

    @pyqtSlot()
    def pBtnRun_Clicked(self):
        hogPeopleDetector = Detector()
        origImage = cv2.imread(self.lEditFileName.text())

        imgSize = np.zeros(3, dtype = "i")
        scaleRange = list()
        imgSize[0] = origImage.shape[0]
        imgSize[1] = origImage.shape[1]
        imgSize[2] = origImage.shape[2]

        if (origImage.shape[1] > 400):
            scale = 400.0 / float(origImage.shape[1])
        else:
            scale = 1.0

        for i in range(self.scales):
            scaleRange.append(scale)
            scale = scale / 1.1

        if(self.processesNum > 1):
            for i in range(self.processesNum, self.scales, 2 * self.processesNum):
                endIdx = i + self.processesNum
                if ( endIdx < self.scales ):
                    scaleRange[i:endIdx] = reversed(scaleRange[i:endIdx])

        scaleRange = np.array(scaleRange)

        start = time.time()
        comm = MPI.COMM_SELF.Spawn(sys.executable,
                                   args = ["detectorWorker.py"],
                                   maxprocs = self.processesNum)        

        comm.Bcast(imgSize, root = MPI.ROOT)
        comm.Bcast(origImage, root = MPI.ROOT)
        comm.Bcast(scaleRange, root = MPI.ROOT)

        boxesFound = np.zeros(self.processesNum, dtype = "i")

        comm.Reduce(None, [boxesFound, MPI.INT], op = MPI.SUM, root = MPI.ROOT)

        results = np.empty([boxesFound.sum(), 5])

        results = comm.gather(results, root = MPI.ROOT)     

        end = time.time()
        print("Time taken: {}".format(end - start))

        comm.Disconnect()

        resultBoxes = results[0]

        for i in range(1, len(results)):
            if (results[i].shape != (0,)):
                resultBoxes = np.concatenate((resultBoxes, results[i]), axis = 0)

        resultBoxes = hogPeopleDetector.non_max_suppression(resultBoxes,
                                                            hogPeopleDetector.suppressionThreshold)

        resultBoxes = hogPeopleDetector.non_max_suppression(resultBoxes,
                                                            hogPeopleDetector.suppressionThreshold2)
        
        peopleCount = len(resultBoxes)

        b,g,r = cv2.split(origImage)      
        imgProccessed  = cv2.merge([r,g,b])

        for box in resultBoxes.astype(int):
            x1 = box[0]
            y1 = box[1]
            x2 = x1 + box[2]
            y2 = y1 + box[3]
            imgProccessed = cv2.rectangle(imgProccessed, (x1,y1), (x2,y2), (0,255,0), 2)

        self.imgDisplayed = QtGui.QImage(imgProccessed,
                                         imgProccessed.shape[1],
                                         imgProccessed.shape[0],
                                         imgProccessed.shape[1] * 3,
                                         QtGui.QImage.Format_RGB888)
        self.imgDisplayed = self.imgDisplayed.scaled(710, 395, QtCore.Qt.KeepAspectRatio)
        self.lImageView.setPixmap(QtGui.QPixmap.fromImage(self.imgDisplayed))

        imgFileName = os.path.basename(self.lEditFileName.text())

        msgBox = QtWidgets.QMessageBox(self)
        msgBox.setText("{} person(s) found in the {} picture.".format(peopleCount, imgFileName))
        msgBox.exec()

    @pyqtSlot()
    def closeEvent(self, evnt):
        for filename in os.listdir(".tmp/"):
            filepath = os.path.join(".tmp/", filename)
            try:
                shutil.rmtree(filepath)
            except OSError:
                os.remove(filepath)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Main()
    app.setStyle(QtWidgets.QStyleFactory.create("Windows"))
    window.show()
    sys.exit(app.exec_())    