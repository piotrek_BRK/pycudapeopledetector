import sys
import os.path
import shutil
import numpy as np
from detector import Detector
import cv2

if __name__ == "__main__":
    hogPeopleDetector = Detector()
    origImage = cv2.imread("test/1.jpg")
    resultBoxes = np.array(hogPeopleDetector.findObjects(origImage))

    ## draw result boxes on image

    for box in resultBoxes.astype(int):
        x1 = box[0]
        y1 = box[1]
        x2 = x1 + box[2]
        y2 = y1 + box[3]
        origImage = cv2.rectangle(origImage, (x1,y1), (x2,y2), (0,255,0), 2)

    print("Found {} persons.".format(resultBoxes.shape[0]))
    cv2.imshow('Results', origImage)
    cv2.waitKey(0)
    